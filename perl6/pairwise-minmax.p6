sub pairwise-minmax(@a) {
    my ($min, $max) = @a[0, 1];
    for @a[2..*] -> $a is copy, $b is copy {
        ($a, $b).=reverse if $a > $b;
        $min = $a if $a < $min;
        $max = $b if $b > $max;
    }
    $min, $max;
}

