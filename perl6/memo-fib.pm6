unit module memo-fib;
use v6.c;

proto proto-fib(Int $n) is export { state @*fib; {*} }
multi proto-fib(Int $ where {$_ == 0|1}) { 1 }
multi proto-fib(Int $n where {$_ > 1}) {
    return @*fib[$n] if @*fib[$n]:exists;
    @*fib[$n] = proto-fib($n - 1) + proto-fib($n - 2);
}

sub seq-fib(Int $n where {$_ ≥ 0}) is export {
    state @fib = 1, 1, * + * ... *;
    return @fib[$n];
}

sub mem-fib(Int $n where {$_ ≥ 0}) is export {
    state %fib;
    return 1 if $n < 2;
    %fib{$n} = &?ROUTINE($n - 1) + &?ROUTINE($n - 2) unless %fib{$n}:exists;
    %fib{$n};
}

