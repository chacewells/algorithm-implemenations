sub factors($n is copy) {
    my $i = 2;
    gather while $n > 1 {
        while $n %% $i {
            take $i;
            $n div= $i;
        }
        $i++;
    }
}

my $n = 100;
say "factors($n) = &factors($n)";
$n = 97;
say "factors($n) = &factors($n)";
$n = 970002711;
say "factors($n) = &factors($n)";

