enum Direction <Left Right>;
class DirectedInt {
    has Int $.value;
    has Direction $.direction = Left;
    method new($value) {
        self.bless(:$value);
    }

    method flip { $!direction = $!direction ~~ Left ?? Right !! Left }
}

sub permute-min-change(@a) {
    my DirectedInt @work = map { DirectedInt.new($_) }, @a;
    my @permutes = $[@work];
    my $last-mobile = True;
    
    while $last-mobile {
        # find largest mobile element k
        my $k;
        for @work.kv -> $i, $v {
            
        }
        # swap k with element it points to
        # reverse direction of elements larger than k
        # add new permutation to the list
    }
}

permute-min-change <1 2 3 4>;
