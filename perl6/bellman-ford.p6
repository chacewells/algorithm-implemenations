sub bellman-ford(@v, Edge @e) {
    for @v -> $v {
        for @e -> $e {
            relax $e
        }
    }

    # detect negative cycles
    my ($u, $v);
    for @e -> $e {
        if $v.d > $u.d + $e {
            # negative cycle
        }
    }
}
