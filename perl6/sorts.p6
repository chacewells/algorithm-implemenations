use v6.c;

for (&bubble-sort-optimized, &selection-sort) -> $algo {
    for 4, * * 2 ... 1024 -> $n {
        my $times = 20;
        my $total-dur = 0;
        for 1..$times {
            my @a = random-array $n;
            my $begin = now;
            $algo(@a);
            $total-dur += now - $begin;
        }
        say "{$algo.name} @ $n: {($total-dur / $times).fmt('%.5f')}s";
    }
}

sub random-array($n) { map {(0..100).roll}, 1..$n }

sub bubble-sort-optimized(@a) {
    for 0..(@a.end - 1) -> $i {
        my $swaps = 0;
        for 0..(@a.end - 1 - $i) -> $j {
            if @a[$j + 1] < @a[$j] {
                @a[$j, $j+1].=reverse;
                ++$swaps;
            }
            last unless $swaps;
        }
    }
}

sub bubble-sort(@a) {
    for 0..(@a.end - 1) -> $i {
        for 0..(@a.end - 1 - $i) -> $j {
            @a[$j, $j+1].=reverse
                if @a[$j + 1] < @a[$j];
        }
    }
}

sub selection-sort(@a) {
    for 0..(@a.end - 1) -> $i {
        my $min = $i;
        for ($i+1)..@a.end -> $j {
            $min = $j if @a[$j] < @a[$min];
        }
        @a[$i, $min].=reverse;
    }
}
