sub insertion-sort(@a) {
    <loop i j v a>.fmt("%s\t").say;
    for 0..@a.end -> $i {
        my ($v, $j) = @a[$i], $i - 1;
        while $j ≥ 0 && @a[$j] > $v {
            @a[$j + 1] = @a[$j];
            ['(while)', $i, $j, $v, @a].fmt("%s\t").say;
            --$j;
        }
        @a[$j + 1] = $v;
        ['(for)', $i, $j, $v, @a].fmt("%s\t").say;
    }
}

# my @a = <89 45 68 90 29 34 17>;
# my @a = <29 29 34 45 68 89 90>; # presorted
my @a = <90 89 68 45 34 29 29>; # reverse sorted

insertion-sort @a;
