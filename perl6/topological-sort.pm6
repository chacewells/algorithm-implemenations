unit module topological-sort;

sub kahn(@adj) is export {
    my $n = @adj.elems;
    for ^$n -> $i {
        my $incoming = [+] map -> $k { @adj[$k;$i] }, ^$n;
        @adj[$i;$i] = $incoming;
    }

    my @out;

    while @out < $n {
        for ^$n -> $i {
            if @adj[$i;$i] == 0 {
                push @out, $i;
                for ^$n -> $k {
                    @adj[$k;$k] -= @adj[$i;$k];
                    @adj[$i;$k] = 0;
                }
                @adj[$i;$i] = -1;
            }
        }
    }

    @out
}

enum Status <PROCESSING UNVISITED VISITED>;

sub DFS(@*adj) is export {
    my Status @*m is default(UNVISITED);
    my Int @*topo;
    for ^@*adj -> $v {
        dfs $v if @*m[$v] == UNVISITED;
    }
    @*topo
}

sub dfs($v) {
    @*m[$v] = PROCESSING;

    for ^@*adj -> $w {
        dfs $w if @*adj[$v;$w] and @*m[$w] != VISITED
    }

    @*m[$v] = VISITED;
    push @*topo, $v;
}

sub s2g($s) is export {
    $s.lines.map({ $[.split(/\s+/)».Int] })
}
