unit module Warshalls-Floyds;

sub warshalls(@w) is export {
    my @last = @w;
    my @curr;

    for ^@w -> $k {
        for ^@w -> $i {
        for ^@w -> $j {
            @curr[$i; $j] = @last[$i; $j] || (@last[$i; $k] && @last[$k; $j])
        } }
        .fmt('%d', "\t").say for @last;
        .join.say given '=' xx 50;
        @last = @curr;
    }
    @last
}

sub floyds(@w) is export {
    my @d = @w;

    for ^@d -> $k {
        for ^@d -> $i {
        for ^@d -> $j {
            @d[$i; $j] = min @d[$i; $j], @d[$i; $k] + @d[$k; $j]
        } }
        .fmt('|%.0f', "\t").say for @d;
        .join.say given '=' xx 50;
    }
    @d
}

sub load-adj(Str $filename where {.IO.f} = 'warshall-data-8.4.txt') is export {
    'warshall-data-8.4.txt'.IO.lines.map({
        $[.split("\t")».Int».Bool]
    })
}

sub load-weights(Str $filename where {.IO.f} = 'floyd-data-8.4.txt') is export {
    $filename.IO.lines.map({
        $[.split("\t")».Num]
    })
}
