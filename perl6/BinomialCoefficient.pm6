unit module BinomialCoefficient;
use v6.c;

sub binomial(\n, \k where k ≤ n) is export {
    my @c;
    for 0 .. n -> \i {
        for 0 .. min(i, k) -> \j {
            @c[i; j] = j == 0|i
                ?? 1
                !! @c[i - 1; j - 1, j].sum;
        }
    }
    @c.map(*.fmt("%5d", "\t")).fmt("%s", "\n").say;
    @c[n; k];
}

sub binomial-n-space($n, $k where $k ≤ $n) is export {
    my @c is default(0) = 1;
    for 1..$n -> $i {
        @c[$_] = @c[$_, $_ - 1].sum for min($i, $k) ... 1
    }
    @c[$k]
}

sub binomial-recurse($*n, $*k where $*k ≤ $*n) is export {
    my @*c;
    br($*n, $*k);
}

sub br($i, $j) {
    unless @*c[$i; $j]:exists {
        @*c[$i; $j] = $j == 0|$i
            ?? 1
            !! br($i - 1, $j - 1) + br($i - 1, $j)
    }
    .map({ ($_ // '_').fmt("%5s") }).join.say for @*c;
    @*c[$i; $j]
}

sub binomial-dp1($n, $k where $k ≤ $n) {
    my @c = 1 xx ($k + 1);
    for ^($n - $k) {
        for 1 .. $k -> $j {
            @c[$j] = @c[$j, $j - 1].sum;
        }
    }
    @c[$k]
}

sub binomial-dp2($n, $k where $k ≤ $n) is export {
    my @c = 1 xx ($k + 1);
     (1 .. $k).map({ @c[$_] = @c[$_, $_ - 1].sum })for ^($n - $k);
    @c[$k];
}

# solve by building a lazy sequence; slow
sub binomial-dp3($n, $k $k ≤ $n) is export {
    ([1 xx ($k + 1)], { [\+] @^a } ... *)[$n - $k][$k]
}

