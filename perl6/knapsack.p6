sub knapsack(@*v, @*w, $cap) {
    my @*f;
    @*f[0] = $[0 xx ($cap + 2)];
    @*f[$_; 0] = 0 for 1 .. @*v.end;
    mf-knapsack(@*v.end, $cap + 1);
}

sub mf-knapsack($i, $j) {
    unless @*f[$i; $j]:exists {
        @*f[$i; $j] = do if $j < @*w[$i] {
           mf-knapsack $i - 1, $j
        } else {
           max( mf-knapsack( $i - 1, $j ), @*v[$i] + mf-knapsack( $i - 1, $j - @*w[$i] ) )
        }
    }
    .say for @*f;
    .join.say given '=' xx 50;
    @*f[$i; $j]
}

my @v = 0, 25, 20, 15, 40, 50;
my @w = 0, 3, 2, 1, 4, 5;
my $cap = 6;

say knapsack @v, @w, $cap;

my @f;
@f[0] = $[0 xx ($cap + 1)];
@f[$_; 0] = 0 for 1 .. @v.end;

for 1..@v.end -> $i {
    for 1..($cap+1) -> $j {
        if $j < @w[$i] {
            @f[$i; $j] = @f[$i - 1; $j];
        } else {
            @f[$i; $j] = max @f[$i - 1; $j], @v[$i] + @f[$i - 1; $j - @w[$i]];
        }
    }
}

.say for @f;
