#include <iostream>
#include <map>

using namespace std;

long long fib(long long n) {
    static map<long long, long long> memo;

    if (n < 2) return 1;
    if ( memo.find(n) == memo.end() )
        memo[n] = fib(n - 1) + fib(n - 2);
    
    return memo[n];
}

int main() {
    for (long long i = 1; i <= 20L; ++i)
        cout << "fib(" << i << ") = " << fib(i) << endl;
    return 0;
}
